# contracode/vimrc

This is a [Vim](https://www.vim.org/) configuration file created for development with Python3.

It adds these enhancements to [Vim](https://www.vim.org/):

- Autocomplete
- An Improved Status Bar
- Unused Whitespace Highlighting
- A Remapped `<Esc>` Key (`jj`, quickly)
- Code Folding (`jk`, quickly)
- A File Browser (`Ctrl-n`)
- Fuzzy File Search (`Ctrl-p`)

### Screenshot

![Vim Screenshot](https://gitlab.com/contracode/vimrc/raw/master/screenshot.png "Vim Screenshot")

### Prerequisites

* [Vim](https://www.vim.org/)
* [Vundle](https://github.com/VundleVim/Vundle.vim)

This `.vimrc` file has been tested using Vim `7.4` and Vim `8.0`, which are installed by default on Ubuntu 16.04 LTS and Ubuntu 18.04 LTS, respectively.

Check the output of `vim --version` to see which version of Vim you have installed.

Within the `vim --version` output, `python` or `python3` support **must** be enabled (i.e., look for `+python` or `+python3`). If both are prefixed with a `-`, you'll need to [build Vim](https://github.com/Valloric/YouCompleteMe/wiki/Building-Vim-from-source) from the [source](https://github.com/vim/vim)!

If you do not have [Vundle](https://github.com/VundleVim/Vundle.vim) yet, it can be installed with:

```bash
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

## Getting Started

- **General**
  - `jj`: When pressed quickly, exits insert mode.
  - `jk`: When pressed quickly, toggles code folding.
- **Appearance**
  - `<F5>`: Toggles between light and dark color schemes.
- **Files & Directories**
  - `Ctrl-n`: Toggles the [NERDTree](https://github.com/scrooloose/nerdtree) file browser.
  - `Ctrl-p`: Toggles [ctrlp.vim](https://github.com/ctrlpvim/ctrlp.vim) fuzzy search.
- **Movement**
  - `Ctrl+k`: Moves up in split mode.
  - `Ctrl+j`: Moves down in split mode.
  - `Ctrl+h`: Moves left in split mode.
  - `Ctrl+l`: Moves right in split mode.
- **Searching**
  - `/foo`: Search and highlight `foo` within the current file.
    - Use `n`/`N` to move forward and backwards, respectively, among matched text.
  - `<Space>hl`: Removes highlight from matched text.

### Installing

1. Copy the `.vimrc` file from this repository directly to your home directory:

   ```bash
   wget https://gitlab.com/contracode/vimrc/raw/master/.vimrc -P ~
   ```
1. Install Vim plugins:

   ```bash
   vim +'PluginInstall' +qa
   ```

1. Build the [YouCompleteMe](https://github.com/Valloric/YouCompleteMe) plugin:

   ```bash
   # Install build dependencies
   sudo apt -y install build-essential cmake
   sudo apt -y install python-dev python3-dev

   # Install the YCM plugin
   cd ~/.vim/bundle/YouCompleteMe
   ./install.py
   ```

1. Install a custom font for [Powerline](https://github.com/powerline/powerline):

   ```bash
   # Download the custom font and its configuration file
   wget https://github.com/powerline/powerline/raw/develop/font/PowerlineSymbols.otf
   wget https://github.com/powerline/powerline/raw/develop/font/10-powerline-symbols.conf

   sudo mv PowerlineSymbols.otf /usr/share/fonts/       # Move the font to valid font path
   fc-cache -vf /usr/share/fonts/                       # Update system font cache
   sudo mv 10-powerline-symbols.conf /etc/fonts/conf.d/ # Install the fontconfig file
   ```

## Built With

**A Vim plugin to install all other plugins:**

- [Vundle](https://github.com/VundleVim/Vundle.vim) - Vim Plugin Installer

**Plugins to enhance the Vim experience:**

- [NERDTree](https://github.com/scrooloose/nerdtree) - For a File Browser
- [Powerline](https://github.com/powerline/powerline) - For an Improved Status Bar

**Plugins for Python development:**

- [ctrlp.vim](https://github.com/ctrlpvim/ctrlp.vim) - For Fuzzy Matching
- [python-syntax](https://github.com/hdima/python-syntax) - For Python Syntax Formatting
- [SimpylFold](https://github.com/tmhedberg/SimpylFold) - For Python Code Folding
- [Syntastic](https://github.com/vim-syntastic/syntastic) - For Python Syntax
- [vim-flake8](https://github.com/nvie/vim-flake8) - For Python PEP-8 Checking
- [YouCompleteMe](https://github.com/Valloric/YouCompleteMe) - For Autocomplete

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/contracode/vimrc/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

To contribute, you will also need to pull this repository, rather than only the `.vimrc` file:

```bash
cd ~
rm .vimrc CONTRIBUTING.md LICENSE README.md screenshot.png
git init .
git remote add origin https://gitlab.com/contracode/vimrc.git
git pull origin master
```

Your first push to this repository will require you to set your upstream branch:

```bash
git push --set-upstream origin master
```

Otherwise, you can [fork this repository](https://gitlab.com/contracode/vimrc/forks/new) and [submit a Merge Request](https://gitlab.com/contracode/vimrc/merge_requests/new).

## Authors

* **Jared Contrascere** - *Initial work* - [contracode](https://contracode.com)

See also, the list of [contributors](https://gitlab.com/contracode/vimrc/project_members) who participated in this project.

## License

This project is licensed under the GPLv3 License - see the [LICENSE](https://gitlab.com/contracode/vimrc/blob/master/LICENSE) file for details

## Acknowledgments

- [Bram Moolenaar](http://moolenaar.net/) - *Author of Vim*
- The authors of the plugins in the *Built With* section - **Thank you!**

If this was helpful to you, **please consider donating** to Bram Moolenaar's charity, [ICCF Holland](http://iccf-holland.org/). 99% of the money they receive goes to help people in the small town of [Kibaale, Uganda](https://goo.gl/maps/on8TFkMbcBE2).

**Donations help AIDS victims.** In this poor area of Africa, many adults are infected with HIV. When they die, orphans are left behind. Bram's charity provides the education and medical support they need.

You'll make the world a better place! Please [give back](http://iccf-holland.org/donate.html)! <sub><sup>(**Opens to the ICCF Holland Donation Page.**)</sup></sub>
