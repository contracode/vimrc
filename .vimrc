""""""""""""""""""""""""
" Plugin Configuration "
""""""""""""""""""""""""
let mapleader=" " " Remap <Leader> key from \ to <Space>
filetype off      " Required by Vundle

" Set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#rc()

" Declare plugins not requiring extra configuration
Plugin 'VundleVim/Vundle.vim'     " Let Vundle manage Vundle (Required)
Plugin 'vim-syntastic/syntastic'  " Add syntax checking on each save
Plugin 'nvie/vim-flake8'          " Add PEP-8 checking on each save
Plugin 'tpope/vim-unimpaired'     " Add quick buffer switching

""""""""""""""""""""""""""
" CtrlP Configuration "
""""""""""""""""""""""""""
Plugin 'ctrlpvim/ctrlp.vim'       " Add a file, buffer, mru, tag, etc. finder
let g:ctrlp_working_path_mode = 'ra'

"""""""""""""""""""""""
" Netrw Configuration "
"""""""""""""""""""""""
" map <C-n> :edit .<CR>
" let g:netrw_liststyle = 3         " Use the tree list view, within netrw
" let g:netrw_banner = 0            " Suppress the directory banner

""""""""""""""""""""""""""
" NERDTree Configuration "
""""""""""""""""""""""""""
Plugin 'scrooloose/nerdtree'            " Add a file tree

let NERDTreeHijackNetrw=1
nnoremap <C-n> :NERDTreeToggle<CR>|     " Open NERDTree with Ctrl+n
let NERDTreeIgnore=['\.pyc$', '\~$']    " Ignore files in NERDTree
let g:NERDTreeDirArrowExpandable = '▸'  " Change the default right arrow
let g:NERDTreeDirArrowCollapsible = '▾' " Change the default down arrow

" Open NERDTree if no file was specified at the command line, or if a
" directory was specified
augroup OpenNERDTreeAutomatically
  autocmd!
  autocmd StdinReadPre * let s:std_in=1
  autocmd VimEnter * if argc() == 0 && !exists("s:std_in") &&
    \  exists(':NERDTree')
    \|   NERDTree
    \| endif
  autocmd StdinReadPre * let s:std_in=1
  autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
augroup END

" Close vim if the only open window is NERDTree
augroup CloseNERDTreeAutomatically
  autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
augroup END

"""""""""""""""""""""""""""
" Powerline Configuration "
"""""""""""""""""""""""""""
Plugin 'powerline/powerline', {'rtp': 'powerline/bindings/vim'}
set laststatus=2                     " Always show the status bar

"""""""""""""""""""""""""""""""
" Python-Syntax Configuration "
"""""""""""""""""""""""""""""""
Plugin 'hdima/python-syntax'
let python_highlight_all=1           " Make Python code look nice

""""""""""""""""""""""""""""
" SimpylFold Configuration "
""""""""""""""""""""""""""""
Plugin 'tmhedberg/SimpylFold'        " Add Python code folding
let g:SimpylFold_docstring_preview=1 " Show docstrings in folded code
nnoremap jk za|                      " Enable folding with j+k

"""""""""""""""""""""""""""""""
" YouCompleteMe Configuration "
"""""""""""""""""""""""""""""""
Plugin 'Valloric/YouCompleteMe' " Add autocomplete
let g:ycm_autoclose_preview_window_after_completion=1
nnoremap <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

" Enable autocomplete within Python Virtual Environments
if has('python')
py << EOF
import os
if 'VIRTUAL_ENV' in os.environ:
  project_base_dir = os.environ['VIRTUAL_ENV']
  activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
  execfile(activate_this, dict(__file__=activate_this))
EOF
elseif has('python3')
py3 << EOF
import os
if 'VIRTUAL_ENV' in os.environ:
  global __file__
  project_base_dir = os.environ['VIRTUAL_ENV']
  __file__ = os.path.join(project_base_dir, 'bin/activate_this.py')
  exec(compile(open(__file__, "rb").read(), __file__, 'exec'), globals(), locals())
EOF
endif

"""""""""""""""""""""
" Vim Configuration "
"""""""""""""""""""""
syntax on
filetype plugin indent on

set encoding=utf-8    " Enable UTF-8 support
set clipboard=unnamed " Copy text to the system clipboard

"-- Files & Directories ---------------------------------------------------
" Tell vim to remember certain things when we exit
"           +-> Remember marks for up to # previously edited files
"           |   +-> Remember global marks
"           |   |  +-> Save up to # lines for each register
"           |   |  |    +-> Remember up to # lines of command-line history
"           |   |  |    |     +-> Number of lines to save from the input line history
"           |   |  |    |     |    +-> Number of lines to save from the search history
"           |   |  |    |     |    |    +-> Disable 'hlsearch' highlighting when starting
"           |   |  |    |     |    |    | +-> Where to save the viminfo files
set viminfo='10,f1,<100,:1000,@100,/100,h,n~/.vim/.viminfo
set nobackup          " Do not make backup
set noswapfile        " Do not make swapfiles

"-- Lines & Columns -------------------------------------------------------
set colorcolumn=80    " Highlight in red if at the 80th column
set number            " Show line numbers

"-- Non-Printing Characters -----------------------------------------------
if has('gui_running')
  set list
  set listchars=tab:▶\ ,eol:★
  set listchars+=trail:◥
  set listchars+=extends:❯
  set listchars+=precedes:❮
  set showbreak=↪\
else
  set list listchars=tab:>-,nbsp:.,trail:.,extends:>,precedes:<
  let &showbreak = '^'
endif

"-- Searching -------------------------------------------------------------
set hlsearch          " Enable color highlighting when searching
set ignorecase        " Use case-insensitive searching by default
set smartcase         " Use case-sensitive searching if a capital letter is entered
set wrapscan          " Wrap around the file when searching

""""""""""""""
" Appearance "
""""""""""""""
"-- Color Scheme ----------------------------------------------------------
Plugin 'jnurmine/Zenburn'                 " Add the Zenburn color scheme
Plugin 'altercation/vim-colors-solarized' " Add the Solarized color scheme


if has('gui_running')                     " Determine which color scheme to load
  set background=dark
  silent! colorscheme solarized
else
  silent! colorscheme zenburn
endif

" Toggle the Solarized dark & light themes by pressing the <F5> key
silent! call togglebg#map("<F5>")

" Revise search term highlight from default
highlight Search cterm=NONE ctermfg=black ctermbg=lightyellow

" Flag Unnecessary Whitespace
highlight BadWhitespace ctermbg=red guibg=darkred

" Disable Background Color Erase (BCE) so that color schemes render properly
" when inside a 256-color tmux and GNU screen.
" See also: http://snk.tuxfamily.org/log/vim-256color-bce.html
if &term =~ '256color'
  set t_ut=
endif

""""""""""""""""
" Key Mappings "
""""""""""""""""
inoremap jj <Esc>`^|       " Remap 'jj' to Escape and leave the cursor in place.

" Redefine split navigations
nnoremap <C-J> <C-W><C-J>| " Use Ctrl+j to move down in split mode
nnoremap <C-K> <C-W><C-K>| " Use Ctrl+k to move up in split mode
nnoremap <C-L> <C-W><C-L>| " Use Ctrl+l to move right in split mode
nnoremap <C-H> <C-W><C-H>| " Use Ctrl+h to move left in split mode

" Un-highlight last search
nnoremap <silent> <leader>hl :set hlsearch!

" Enable mouse positioning with the cursor, as well as visual select and scroll
" with the mouse
if has('mouse')
  set mouse=a
endif

""""""""""""""""
" Autocommands "
""""""""""""""""
" Enable PEP-8 indentation
autocmd FileType *.py
        \ set tabstop=4|
        \ set softtabstop=4|
        \ set shiftwidth=4|
        \ set textwidth=79|
        \ set expandtab|
        \ set autoindent|
        \ set fileformat=unix|

" Set different tab preferences for full stack development
autocmd FileType *.js, *.html, *.css
        \ set tabstop=2|
        \ set softtabstop=2|
        \ set shiftwidth=2|

" Flag unnecessary whitespace
autocmd FileType *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

